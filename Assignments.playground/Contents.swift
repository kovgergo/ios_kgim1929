//: Playground - noun: a place where people can play

//iOS Programozás, választható tantárgy 2022
//1. Labor Playground és bevezetés a Swift nyelvbe

import Foundation

// VARIABLES

// 1. (0.5 p) Declare a constant array favouriteNumbers with values for your favourite numbers.

let favouriteNumbers = [1, 7, 8, 123]

print(favouriteNumbers)


// 2. (0.5 p) Declare a dictionary with the name of your colleagues and the "department" he/she is working on.

var myColleagues = ["Aron": "Testing",
            "Zsolt": "Cloud",
            "Anna": "Automation",
            "Panna": "Cloud"]
print(myColleagues)

// 3. (0.5 p) Declare an array of tuples with the list of your colleagues and their height.
// ex. tuple: ("Big Guy", 201)

var colleaguesHeight = [("Anna", 175), ("Balint", 178), ("Zsolt", 180), ("Bela", 180), ("Zoltan", 185), ("Panna", 173)]
print(colleaguesHeight)

// 4. (0.5 p) Order the array created above in ascending order (based on height)
// use `sorted`

var colleaguesHeightSorted = colleaguesHeight.sorted(by: {$0.1 < $1.1})
print(colleaguesHeightSorted)

// 5. (0.5 p) Group the dictionary by department.
// use the: `Dictionary(grouping: ..., by: ...)` initializer

var myColleaguesByDepartment = Dictionary(grouping: myColleagues, by: {$0.value})
print(myColleaguesByDepartment)

// 6. (0.5 p) Order the tuple based on the heights, if there are entries with the same height, order them by alphabetical order.
// hint: the `sorted` function can take two propeties as parameter

colleaguesHeightSorted = colleaguesHeight.sorted(by: {$0.1 == $1.1 ? $0.0 < $1.0 : $0.1 < $1.1})
print(colleaguesHeightSorted)

// 7. (0.5 p) Increment every number by one in favouriteNumbers.
// use `map`

let incremented = favouriteNumbers.map {$0 + 1}
print(incremented)

// 8. (0.5 p) Print out only the name of your colleagues from the dictionary.

print(myColleagues.keys)

// 9. (1 p) Print out the average height of your colleagues.
// use `map` + `reduce`



// 10. (1 p) Split the array in half.
// hint: you can access a subarray like this: `someArray[..<n]` or `someArray[n...]`



// 11. (1 p) Declare the first 10 Fibonacci numbers in a set. (1,1,2,3,...)
// Obviously the number 1 will appear only once in this set (properties of a set)
// write the algorithm, (implement a function like this: `func fibonacci(n: Int) -> Set<Int>`)



// 12. (1 p) Get the intersect of two sets: the first 10 Fibonacci numbers and the first 10 prime numbers.
// This time you won't need to write the alorithm of the Fibonacci numbers. Just put them in a list, and use that list.
// there is a method `intersection`



// CONTROL FLOW

// 13. (1 p) Take a positive number and print the reversed version of it.
// write the algorithm (implement a function like this: `func reverse(number: Int) -> Int`)



// 14. (1 p) Choose a number and find out if it's palindrome.
// use the solution from above


// CLASSES AND STRUCTS

// 15. (1 p) Write a struct named Course which will have a String property name and an Int one mark.
// ex. struct Sth { aProperty: Type, ..}



// 16. (1 p) Create a class named Student which will have a private String property name and a private array of courses which he attends to. Both properties will be initialized in the constructor.



// ENUMS

// 17. (1 p) Create an enum type which will have two cases: normal and scholar.
// ex. enum Sth { case fist, case last }



// 18.( 1 p) Create a String property in the enum (e.g. description) which will return a textual representation of each case (e.g. "This is a scholar student.").
// use the `switch` statement for setting the right textual representation



// 19. (1 p) Add this enum as a property to the above mentioned Student class.
// 20. (1 p) Modify the Student class constructor so the type type parameter has a default value of .normal).
// HINT: you can give a default value in the constructor like `init(..., type: Type = .aType)`



// PROTOCOLS

// 21. (1 p) Vehicles can have different properties and functionality.
// All Vehicles:
// • Have a speed at which they move
// • Calculate the duration it will take them to travel a certain distance
// All Vehicles except a Motorcycle
// • Have an amount of Windows
// Only Buses:
// • Have a seating capacity
// Create the following Vehicles types: Car, Bus, Motorcycle. Do not use subclassing, use protocols instead. Create an array over the minimum required protocol and put an instance of every type in it and print the the following text for every item of the array (if it's possible): " *type* has *amount of windows* windows and needs *time* to travel 100 kilometers. "

protocol Vehicle {
    var speed: Double { get set }
    mutating func calculateDuration(distance: Double) -> Double
}

protocol WindowVehicle: Vehicle {
    // TODO: add the needed property(s)
}

struct Bus: WindowVehicle {
    var speed: Double
    func calculateDuration(distance: Double) -> Double {
        return distance * 60 / speed
    }
    
    // TODO: implement what's needed and add the needed property(s)
}

// TODO: implement the other types


// TODO: add elements to this array
var vehicles: [Vehicle] = []


// EXTENSTIONS

// 22. (1 p) Create Int extensions for:
// • Radian value of a degree (computed property)
// • Array of digits of Int (computed property)
extension Int {
    
    func toRadian () -> Double {
        return Double.pi * Double(self) / 180
    }
    
    // TODO: Array of digits
}

// 23. (1 p) Create String extensions for:
// • Check if the string contains the character 'a' or 'A'





// 24. (1 p) Create Date extensions for:
// • Check if a date is in the future (computed property)
// • Check if a date is in the past (computed property)
// • Check if a date is in today (computed property)

extension Date{
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    // TODO: is in the future
    
    // TODO: is in the past
}

let formatter = DateFormatter()
formatter.dateFormat = "yyyy/MM/dd"
let date: Date = formatter.date(from: "2018/03/20")!
let today = Date()
// TODO: test the written extension



// CLOSURES

// 25. (1 p) Declare a closure that takes an Integer as an argument and returns if the number is odd or even (using regular syntax and shorthand parameter names)
// ex: `let empty: (String) -> Bool = { $0.isEmpty }` - closure for testing if a string is empty



// 26. (1 p) Use the above defined closure to filter out odd numbers from an array of random numbers (use the filter function)
func makeRandomList() -> [Int] {
    return [1, 43, 654, 32, 67, 23, 78, 94, 12, 67]
}
// TODO: do the filtering



// 27. (1 p) Declare a closure that takes 2 Integers as parameters and returns true if the first argument is larger than the second and false otherwise (using regular syntax and shorthand parameter names)







